package com.springbootbackend.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.springbootbackend.model.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {

	

}
